﻿#include "Function.h"
#include "Common.h"

int main(void)
{
	char menu;

	do {
		menu = Menu();
		switch (menu)
		{
		case '1': Input(menu); break;
		case '2': DeleteInfo(menu); break;
		case '3': SearchInfo(menu); break;
		case '4': ListAll(); break;
		default: puts("잘못 입력하셨습니다!\n");
		}
	} while (menu != '0');

	return 0;
}