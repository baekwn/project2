﻿#ifndef __FUNCTION_H__
#define __FUNCTION_H__

#define STUDENTMAX 10

//학생성적 정보 구조체
typedef struct student {
	char name[20]; //학생이름
	char korean[5];//국어성적
	char english[5];//영어성적
	char math[5];//수학성적
} Student;

char Menu(void);//메뉴 출력
void Input(void);//성적 입력
void DeleteInfo(void);//삭제
void SearchInfo(void);//검색
void List(int);//학생성적 출력
void ListAll(void);//모든 학생의 성적 출력

#endif