﻿#include "Function.h"
#include "Common.h"

Student arr[STUDENTMAX];
int student_number = 0;

/*메뉴출력 및 선택*/
char Menu(void)
{
	char menu[10];

	puts("\n----------------------------------------------------------------------------");
	puts("성적 관리 프로그램<0:종료>");
	puts("1: 학생 데이터 입력\n2: 학생 데이터 삭제\n3: 학생 검색\n4: 목록 보기\n");

	gets(menu);

	return menu[0];
}

/*
*입력
*학생성적 정보입력
*/
void Input(void)
{
	puts("이름 입력"); gets(arr[student_number].name);
	puts("국어 성적 입력"); gets(arr[student_number].korean);
	puts("영어 성적 입력"); gets(arr[student_number].english);
	puts("수학 성적 입력"); gets(arr[student_number].math);

	student_number++;
}

/*
*학생 정보 삭제
*학생의 이름을 입력받아 해당 학생정보 삭제
*/
void DeleteInfo(void)
{
	char search_name[20];
	int i,k;

	puts("삭제할 학생의 이름을 입력하세요");
	gets(search_name);

	for (i = 0; i < student_number; i++)
	{
		if (!strcmp(search_name, arr[i].name))
		{
			for (k = i; k < student_number-1; k++) {
				arr[k] = arr[k + 1];
			}
			student_number--;
			break;
		}

		if (i == (student_number - 1))
			puts("없는 학생 입니다.");
	}
}

/*
*학생 정보 검색
*학생의 이름을 입력받아 해당학생 성적 출력
*/
void SearchInfo(void)
{
	char search_name[20];
	int i;

	puts("검색할 학생의 이름을 입력하세요.");
	gets(search_name);

	for (i = 0; i < student_number; i++)
	{
		if (!strcmp(search_name, arr[i].name))
		{
			List(i); break;
		}

		if (i == (student_number - 1))
			puts("없는 학생 입니다.");
	}
}

/*학생성적 출력*/
void List(int number)
{
	printf("\n이름 : %s\n", arr[number].name);
	printf(" 국어 점수 : %s\n", arr[number].korean);
	printf(" 영어 점수 : %s\n", arr[number].english);
	printf(" 수학 점수 : %s\n", arr[number].math);
}

/*모든 학생들의 출력*/
void ListAll(void)
{
	int k;

	for (k = 0; k < student_number; k++) {
		List(k);
	}
}